using System;

namespace ComSci572Project
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (CS572Game game = new CS572Game())
            {
                game.Run();
            }
        }
    }
#endif
}

