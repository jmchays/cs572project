using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace ComSci572Project
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Adventurer : Microsoft.Xna.Framework.DrawableGameComponent
    {
        CS572Game game;
        Texture2D texture;
        Vector2 position;
        float horizontalSpeed = 4;
        float verticalSpeed = 4;
        Point frameSize = new Point(32, 48);
        Point currentFrame = new Point(0, 0);
        Point sheetSize = new Point(4, 4);

        public Adventurer(Game game)
            : base(game)
        {
            this.game = game as CS572Game;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            base.Initialize();
            texture = Game.Content.Load<Texture2D>(@"Images\adventurer");
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            ++currentFrame.X;
            if (currentFrame.X >= sheetSize.X)
            {
                currentFrame.X = 0;
                ++currentFrame.Y;

                if (currentFrame.Y >= sheetSize.Y)
                {
                    currentFrame.Y = 0;
                }
            }

            position.X += horizontalSpeed;
            position.Y += verticalSpeed;
            if (position.X > Game.Window.ClientBounds.Width - frameSize.X || position.X < 0)
            {
                horizontalSpeed *= -1;
            }
            if (position.Y > Game.Window.ClientBounds.Height - frameSize.Y || position.Y < 0)
            {
                verticalSpeed *= -1;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            game.SpriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);

            game.SpriteBatch.Draw(texture, position, new Rectangle(currentFrame.X * frameSize.X, currentFrame.Y * frameSize.Y, frameSize.X, frameSize.Y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);

            game.SpriteBatch.End();

            base.Draw(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }
    }
}
